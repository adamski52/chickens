# Learning to Unity Good

## Background
I've never used Unity, at all.  I'd like to learn it.

I'm familiar with C# on a basic level but am enough of a noob that I had to learn about `[,]` instead of `[][]` and `:base(...)` instead of `super(...)`.

I have an extensive background in programming languages and since it's Unity I could have chosen my strongest language (JavaScript), but I wanted to learn something new whle I was at it.

## Game Concept
I wanted to start simple, and so I thought "what exemplifies simple". My answer was "why did the chicken cross the road", and so here we are.

The purpose of the game will be like Frogger, except every move you make will damage/eliminate the tile you just used.  Meaning if you jump into the road and notice you're about ot get trucked and try to jump back, you may find that the tile you're jumping back to is gone and that's game over.

## Project Concept
The purpose of the project at large is to demonstrate **how to learn**.  Things like:
* When does it make sense to start unit testing, given you have no idea what your destination even is?
* When does it make sense to prototype or aim for a full implementation?
* What is necessary to sit down and learn vs. what can be winged on the fly given previous experience?
* How can the above be communicated in some fashion for a future talk/demonstration?

## Change notes per commit
I will try to keep the following up to date with each commit.  Browing through commits of this file ought to show the progression of ideas, but I will also try to keep legacy bulllet points so that the latest always shows the entire history.  They are listed in order, so the first is at the top.

1) I have no idea what I'm doing, but it may be beneficial to try to force Unity/C# patterns in to patterns I do know so that when I have enough solid foundation, I'm comfortable that the pattern is extensible.  I learned about MonoBehaviours and ScriptableObjects, but I worry they're crutches that clutter up game scenes and what amounts to the global scope.  They also require manual wiring of dependencies which I hope to one day eliminate.

2) I wound up making everything a MonoBehaviour and now I understand why that's not necessary.  ScriptableObjects are probably better solutions for long-lived things but those require special decorators and duplicated instances and that seems unnecessary.  I'd rather use the trusty service pattern and pass instances into methods instead of creating new instances for everything.  I do not know a lot about game programming but I do know that creating/destroying instances is etremely expensive; I can see myself recycling tiles a lot if this ever makes it to optimization.

3) I learned that composition is a much better and easier solution than inheritance.  This is the first major refactor away from inheritance.

4) I learned how to test things a little bit.  I'm confident that I can write code in a pattern that will be easily testable in the future.  I also learned how Unity does package management.  It's a little clunky but it's neat that they're extensions to the IDE instead of just code libraries.  I bet the future of development leans more toward task-specific IDEs instead of the monstrosities that are Maven, Gradle, NPM etc.  Android and iOS figured this out many years ago.  Web Development is always years behind.

5) I should not bother testing too much right now, since every time I work on this it seems to involve some sort of TNT detonating on whatever I just worked on.  Testing is good but not if you're testing something you're going to entirely throw away in a day.

6) I'm ready to try to make an actual level instead of just a 10x10 grid of the same tile.  This will require multiple types and a way to represent these.  I looked into TileMaps but I don't think those lend themselves well to the effect I'd like to create with tiles disappearing and being tangible 3D objects.  Instead, I'm opting for a class which houses all of the different prefabs and provides an Instantiate abstraction.  I also need to get in the habit of using `Time.deltaTime` everywhere, although I do not know if the Tweening library I'm using already does so.

7) Using prefabs with properties I was able to create versions with different strengths.  So a mud tile has a health of 1, a grass tile a health of 2, etc.  I'm still prototyping so I havent bothered with anything but a solid material but these different types can be used to skin them separately.  By swapping the texture, I'll be able to represent different states of the same type (grass of health 2, grass of health 1, etc).  Next up, I want to learn about doing a UI layer to represent a 'start' screen, 'continue' screen etc.

8) Started playing around with camera controls.  Panning was easy enough but rotation is weird.  I bet it'll be easier to parent everything to a master gameobject at the root and rotate the game object instead of actually moving the camera around.  Otherwise the origin of the camera matters too much and it gets impossible.
