﻿using UnityEngine;
using DG.Tweening;

public class MovementService : GameObjectWrapper
{
    private MapService mapService;
    private InstantiationService instantiationService;
    private GameStateService gameStateService;
    private int currentX;
    private int currentY;
    private bool isMoving = false;
    private float fixedZ = 1.0f;

    public MovementService(MapService mapService, GameStateService gameStateService, InstantiationService instantiationService, GameObject playerPrefab) : base(MonoBehaviour.Instantiate(playerPrefab, new Vector3(0, 1, 0), Quaternion.identity))
    {
        this.currentX = 0;
        this.currentY = 0;
        this.mapService = mapService;
        this.gameStateService = gameStateService;
        this.isMoving = false;
        this.instantiationService = instantiationService;
        this.instantiationService.AddInstance(gameObject);
    }

    public void Reset(int x, int y)
    {
        this.currentX = x;
        this.currentY = y;
        isMoving = false;

        // TODO: This is definitely a bug. FixedZ should work instead of .125f.
        gameObject.transform.position = new Vector3(x, .125f, 0);
        gameObject.GetComponent<Renderer>().material.DOFade(1, .5f);
    }

    private void MoveTo(Vector3 moveToPos)
    {
        // TODO: Need to figure out how to fix this origin BS
        moveToPos.y = fixedZ;
        moveToPos.x += .35f;
        //moveToPos.z += .5f;
        gameObject.transform.DOLocalJump(moveToPos, 1, 1, .5f).OnComplete(() =>
        {
            Tile currentTile = mapService.GetTiles()[currentX, currentY];
            if (!currentTile.IsSteppable())
            {
                gameObject.transform.DOLocalMoveY(-10, 1f);
                gameObject.GetComponent<Renderer>().material.DOFade(0, 1f).OnComplete(() =>
                {
                    gameStateService.SetGameOver();
                });
            }
            isMoving = false;
        });
    }

    public void Update()
    {
        if(isMoving)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            MoveNorth();
            return;
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            MoveWest();
            return;
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            MoveSouth();
            return;
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            MoveEast();
            return;
        }
    }

    public void MoveNorth()
    {
        if (currentY < mapService.GetTiles().GetLength(0) - 1)
        {
            UpdatePosition(0, 1);
        }
    }

    public void MoveSouth()
    {
        if (currentY > 0)
        {
            UpdatePosition(0, -1);
        }
    }

    public void MoveEast()
    {
        if (currentX < mapService.GetTiles().GetLength(1) - 1)
        {
            UpdatePosition(1, 0);
        }
    }

    public void MoveWest()
    {
        if (currentX > 0)
        {
            UpdatePosition(-1, 0);
        }
    }

    private void UpdatePosition(int deltaX, int deltaY)
    {
        isMoving = true;
        mapService.HandleStep(currentX, currentY);
        currentX += deltaX;
        currentY += deltaY;

        MoveTo(mapService.GetTiles()[currentX, currentY].GetPosition());
    }
}
