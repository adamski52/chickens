﻿public class GameStateService
{
    public enum GameOverState
    {
        GAME_OVER,
        GAME_ON
    }

    private GameOverState state = GameOverState.GAME_ON;

    public void SetGameOn() {
        state = GameOverState.GAME_ON;
    }

    public void SetGameOver()
    {
        state = GameOverState.GAME_OVER;
    }

    public bool IsGameOver()
    {
        return state == GameOverState.GAME_OVER;
    }

    public bool IsGameOn()
    {
        return state == GameOverState.GAME_ON;
    }
}
