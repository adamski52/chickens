﻿using UnityEngine;

public class InstantiationService
{
    private GameObject parent;

    public GameObject GetGameObject()
    {
        return parent;
    }

    public void AddInstance(GameObject instance) {
        parent = GameObject.FindGameObjectWithTag("GameController");
        instance.transform.parent = parent.transform;
    }
}
