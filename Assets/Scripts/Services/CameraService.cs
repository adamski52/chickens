﻿using UnityEngine;
using UnityEngine.UIElements;

public class CameraService : GameObjectWrapper
{
    private InstantiationService instantiationService;
    private float panSpeed = 7.0f;
    private float zoomSpeed = 1000.0f;
    private float rotateSpeed = 10.0f;
    private float edgeBorder = 20f;
    private float minX = -10;
    private float maxX = 10;
    private float minY = -10;
    private float maxY = 10;
    private float minZoom = 1;
    private float maxZoom = 12;

    public CameraService(InstantiationService instantiationService, GameObject gameObject) : base(gameObject) {
        this.instantiationService = instantiationService;
        gameObject.transform.position = new Vector3(3, 3, -2);
        gameObject.transform.rotation = Quaternion.Euler(new Vector3(35, -35, 0));
        //gameObject.transform.position = new Vector3(1, 7, .5f);
        //gameObject.transform.rotation = Quaternion.Euler(new Vector3(45, 0, 0));
    }

    public void Update()
    {
        return;
        bool handled = UpdateRotation() || UpdatePosition();
    }

    private bool UpdateRotation()
    {
        if (!Input.GetMouseButton((int)MouseButton.LeftMouse))
        {
            return false;
        }

        Quaternion rotation = instantiationService.GetGameObject().transform.rotation;

        float xMovement = Input.GetAxis("Mouse X");
        float yMovement = Input.GetAxis("Mouse Y");

        rotation.y -= xMovement * rotateSpeed * Time.deltaTime;
        //rotation.x -= yMovement * rotateSpeed * Time.deltaTime;

        instantiationService.GetGameObject().transform.rotation = rotation;

        return true;
    }

    
    private bool UpdatePosition() {
        Vector3 position = gameObject.transform.position;
        
        if(Input.mousePosition.y > Screen.height - edgeBorder)
        {
            position.z += panSpeed * Time.deltaTime;
        }
        else if (Input.mousePosition.y < edgeBorder)
        {
            position.z -= panSpeed * Time.deltaTime;
        }

        if(Input.mousePosition.x > Screen.width - edgeBorder)
        {
            position.x += panSpeed * Time.deltaTime;
        }
        else if(Input.mousePosition.x < edgeBorder)
        {
            position.x -= panSpeed * Time.deltaTime;
        }

        float scrollAmount = Input.GetAxis("Mouse ScrollWheel");
        position.y -= scrollAmount * zoomSpeed * Time.deltaTime;

        position.x = Mathf.Clamp(position.x, minX, maxX);
        position.y = Mathf.Clamp(position.y, minZoom, maxZoom);
        position.z = Mathf.Clamp(position.z, minY, maxY);

        gameObject.transform.position = position;

        return true;
    }
}
