﻿using System;
using UnityEngine;
public class MapService
{
    private Tile[,] currentMap = null;
    private InstantiationService instantiationService;

    public MapService(InstantiationService instantiationService)
    {
        this.instantiationService = instantiationService;
    }

    public void MakeMap(GameObject[,] tiles)
    {
        // TODO:  Cache these tiles so they dont need to be destroyed/instantiated repeatedly

        if (currentMap != null)
        {
            for (int x = 0; x < currentMap.GetLength(0); x++)
            {
                for (int y = 0; y < currentMap.GetLength(1); y++)
                {
                    MonoBehaviour.Destroy(currentMap[x, y].GetGameObject());
                }
            }
        }

        currentMap = new Tile[tiles.GetLength(0), tiles.GetLength(1)];

        for(int x = 0; x < tiles.GetLength(0); x++)
        {
            for(int y = 0; y < tiles.GetLength(1); y++)
            {
                GameObject instance = MonoBehaviour.Instantiate(tiles[x, y], GetPosition(tiles[x, y], x, y), Quaternion.identity);
                instantiationService.AddInstance(instance);
                Tile tile = new Tile(instance);
                currentMap[x, y] = tile;
            }
        }
    }

    private Vector3 GetPosition(GameObject tile, int x, int y)
    {
        return new Vector3(tile.transform.localScale.x * x, 0, tile.transform.localScale.z * y);
    }

    public Tile[,] GetTiles()
    {
        return currentMap;
    }

    public void HandleStep(int x, int y)
    {
        Tile tile = GetTiles()[x, y];
        tile.HandleStep();
    }
}
