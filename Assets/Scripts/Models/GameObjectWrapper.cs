﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectWrapper
{
    protected GameObject gameObject;
    public GameObjectWrapper(GameObject gameObject)
    {
        this.gameObject = gameObject;
    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }
}
