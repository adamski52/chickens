﻿using UnityEngine;
using DG.Tweening;

public class Tile : GameObjectWrapper
{
    public Tile(GameObject gameObject) : base(gameObject) {}

    public Vector3 GetPosition()
    {
        return gameObject.transform.position;
    }

    public bool IsSteppable()
    {
        return GetHealth() > 0;
    }

    public int GetHealth()
    {
        return gameObject.GetComponent<TileBehavior>().health;
    }

    public void HandleStep()
    {
        gameObject.GetComponent<TileBehavior>().health--;
        if (!IsSteppable())
        {
            gameObject.transform.DOLocalMoveY(-10, 1f);
            gameObject.GetComponent<Renderer>().material.DOFade(0, 1f);
        }
    }
}
