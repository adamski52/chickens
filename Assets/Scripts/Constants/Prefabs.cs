﻿using UnityEngine;

public class PREFABS
{
    public static class TILES
    {
        public static GameObject GRASS = (GameObject)Resources.Load("Tiles/Grass");
        public static GameObject MUD = (GameObject)Resources.Load("Tiles/Mud");
        public static GameObject ROAD = (GameObject)Resources.Load("Tiles/Road");
    }

    public static class CHARACTERS
    {
        public static GameObject PLAYER = (GameObject)Resources.Load("Characters/Chicken");
    }
}
