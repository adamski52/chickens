﻿using UnityEngine;
using System;

public class GameBehavior : MonoBehaviour
{

    private MapService mapService;
    private MovementService movementService;
    private CameraService cameraService;
    private GameStateService gameStateService;
    private InstantiationService instantiationService;
    public GameObject gameOverMenu;

    public void Start()
    {
        gameStateService = new GameStateService();
        instantiationService = new InstantiationService();
        mapService = new MapService(instantiationService);

        // TODO: May want to use a Controller pattern for this intead
        // TODO: Parent this to a blank game object and use that instead
        cameraService = new CameraService(instantiationService, GameObject.FindGameObjectWithTag("MainCamera"));

        // TODO: May want to use a Controller pattern for this instead
        movementService = new MovementService(mapService, gameStateService, instantiationService, PREFABS.CHARACTERS.PLAYER);

        OnStart();
    }
    
    void OnStart()
    {
        gameStateService.SetGameOn();
        mapService.MakeMap(new GameObject[,] {
           { PREFABS.TILES.ROAD, PREFABS.TILES.GRASS, PREFABS.TILES.MUD},
           { PREFABS.TILES.ROAD, PREFABS.TILES.GRASS, PREFABS.TILES.MUD},
           { PREFABS.TILES.ROAD, PREFABS.TILES.GRASS, PREFABS.TILES.MUD}
        });


        movementService.Reset(0, 0);
    }

    public void Update()
    {
        if (gameStateService.IsGameOn())
        {
            movementService.Update();
            cameraService.Update();
        }

        gameOverMenu.SetActive(gameStateService.IsGameOver());
    }

    public void OnTryAgain()
    {
        OnStart();
    }

    public void OnQuit()
    {
        Application.Quit();
    }
}
